#include <stdio.h>

int I;
char C;
long L;
short S;  
double D;

main(){
  int i;
  char c;
  long l;
  short s;  
  double d;
  
  printf("int I:\nAdres: %p | liczba bajtow: %d\n\n", &I, sizeof(I));
  printf("char C:\nAdres: %p | liczba bajtow: %d\n\n", &C, sizeof(C));
  printf("long L:\nAdres: %p | liczba bajtow: %d\n\n", &L, sizeof(L));
  printf("short S:\nAdres: %p | liczba bajtow: %d\n\n", &S, sizeof(S));
  printf("double D:\nAdres: %p | liczba bajtow: %d\n\n", &D, sizeof(D));
  printf("int i:\nAdres: %p | liczba bajtow: %d\n\n", &i, sizeof(i));
  printf("char c:\nAdres: %p | liczba bajtow: %d\n\n", &c, sizeof(c));
  printf("long l:\nAdres: %p | liczba bajtow: %d\n\n", &l, sizeof(l));
  printf("short s:\nAdres: %p | liczba bajtow: %d\n\n", &s, sizeof(s));
  printf("double d:\nAdres: %p | liczba bajtow: %d\n\n", &d, sizeof(d));
}
